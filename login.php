<?php

    session_start();
    if (isset($_SESSION['userId']) && $_SESSION['userId'] != '') {
        header ("Location: index.php");
    }


$user = "";
$pass = "";
$errorMessage = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$user = $_POST['username'];
	$pass = $_POST['password'];
    $passHashed = md5($pass);

	require('db.php');

    $SQL = "SELECT * FROM users WHERE username = '$user' AND pass = '". $passHashed ."'";
    $errorMessage =  $SQL;
    $result = mysqli_query($conn, $SQL);
    $num_rows = $result->num_rows;

    if ($result) {
        if ($num_rows > 0) {
            $row = $result->fetch_assoc();
            extract($row);
            session_start();
            $_SESSION['userId'] = $id;
            header ("Location: index.php");
        }
        else {
            $errorMessage = "No such username and password";
        }
    }
    else {
        $errorMessage = "Code erorr";
    }

    $conn->close();
}

?>

    <html>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
        <link rel="stylesheet" href="bootswatch/Cyborg/bootstrap.min.css" />
        <link rel="stylesheet" href="site.css" />

        <title>Login Page</title>
    </head>

    <body>

        <div class="container-fluid">
            <div class="col-md-4 col-md-offset-4">
                <div class="formWrapper">
                    <form method="POST" action="login.php">
                        <h3>Login to chat</h3>
                        <br />
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <input class="form-control" type='TEXT' name='username' maxlength="20" required />
                        </div>
                        <div class="form-group">
                            <label for="password">Password: </label>
                            <input class="form-control" type='password' name='password' maxlength="20" required />
                        </div>
                        <div class="form-group">
                            <input class="form-control btn btn-warning" type="Submit" value="Login" />
                        </div>
                        <a href="register.php">Click to register</a>
                        <p id="errorMessage">
                            <?php echo $errorMessage; ?>
                        </p>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

    </body>

    </html>