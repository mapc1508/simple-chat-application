<?php
include_once 'messageClass.php';
require('db.php');

$data = json_decode(file_get_contents("php://input"));
$lastId = $data->lastId;
$sender = $data->sender;
$receiver = $data->receiver;

$messages = array();

$SQL = "SELECT * FROM messages
        WHERE id > " . $lastId . " AND ((sender = " . $sender . " AND receiver = " . $receiver . ") 
        OR (sender = " . $receiver . " AND receiver = " . $sender . ")) ORDER BY id";

    $result = mysqli_query($conn, $SQL);
    $num_rows = $result->num_rows;

    if ($num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            extract($row);
            $message = new Message();
            $message->sender = $sender;
            $message->content = $content;
            $message->receiver = $receiver;
            $message->id = $id;
            $message->sendingTime = $sendingTime;
            array_push($messages,$message);
        }
        $json = json_encode( (array)$messages);
    }
    else {
        $json = json_encode( (array)"");
    }
    

$conn->close();
echo $json;

?>