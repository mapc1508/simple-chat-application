
<?php 
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

include_once 'messageClass.php';

$message = new Message();

$message->sender = $data->sender;
$message->receiver = $data->receiver;
$message->content = $data->sentMessage;
$message->sendingTime = $data->sendingTime;

$json = json_encode( (array)$message );

require('db.php');

$sql = "INSERT INTO messages(sender, receiver, content, sendingTime) VALUES (?,?,?,?);";
$prepStat = $conn->prepare($sql);
$prepStat->bind_param("iiss", $message->sender, $message->receiver, $message->content, $message->sendingTime);
$prepStat->execute();
$prepStat->close();
$conn->close();

echo $json;


?>