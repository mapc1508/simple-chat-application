<?php
require('db.php');

    $username = "";
    $password = "";
    $errorMessage = "";

if($_SERVER['REQUEST_METHOD'] === 'POST'){

    $username = $_POST['username'];
    $password = $_POST['password'];
    $errorMessage = "";
    $passwordHashed = md5($password);

    $sql = "INSERT INTO users (username, pass) VALUES (?,?);";
    $prepStat = $conn->prepare($sql);
    $prepStat->bind_param("ss", $username, $passwordHashed);
    $prepStat->execute();

    if($prepStat->errno == 1062){
        $errorMessage = "Username has already been taken.";
    }
    
    if($prepStat->errno == 0){
        header("Location: login.php");
    }

    if($username == "" || $password == "") $errorMessage = "All fields are required";

    $prepStat->close();
    $conn->close();
}
?>

    <html>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
        <link rel="stylesheet" href="bootswatch/Cyborg/bootstrap.min.css" />
        <link rel="stylesheet" href="site.css" />
        <title>Register Page</title>
    </head>

    <body>

        <div class="container-fluid">
            <div class="col-md-4 col-md-offset-4">
                <div class="formWrapper">
                <form method="POST" action="register.php" id="login">
                        <h3>User registration</h3>
                        <br />
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <input class="form-control" type='text' name='username' maxlength="20" required />
                        </div>
                        <div class="form-group">
                            <label for="password">Password: </label>
                            <input class="form-control" type='password' name='password' maxlength="20" required/>
                        </div>
                        <div class="form-group">
                            <input class="form-control btn btn-warning" type="Submit" value="Register" />
                        </div>
                        <a href="login.php">Click to login</a>
                        <p id="errorMessage">
                            <?php echo $errorMessage; ?>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
        <script>

            $("#login").validate({
                rules: {
                    'password': {
                        minlength: 6
                    },
                    'username': {
                        minlength: 4
                    }
                }
            });
        </script>
    </body>

    </html>