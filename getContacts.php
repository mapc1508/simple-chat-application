<?php

session_start();

include_once 'userClass.php';
require('db.php');
$SQL = "SELECT * FROM users WHERE id <> " . $_SESSION['userId'];

$users = array();

$result = mysqli_query($conn, $SQL);
$num_rows = $result->num_rows;

if ($num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        extract($row);
        $user = new User();
        $user->username = $username;
        $user->id = $id;
        array_push($users,$user);
    }
    $json = json_encode( (array)$users);
}
else {
    $json = json_encode( (array)"");
}

$conn->close();
echo $json;
?>