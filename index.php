<?php session_start();
    if (!(isset($_SESSION['userId']) && $_SESSION['userId'] != '')) {
        header ("Location: login.php");
    }
?>

    <html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chat App</title>
        <link rel="shortcut icon" href="chat_icon.png">


        <!--Stylesheets-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="bootswatch/Cyborg/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="site.css" />

        <!--Scripts-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="angular.min.js"></script>

    </head>

    <body>

        <div class="col-md-8 col-md-offset-2">
            <div class="container-fluid formWrapper" ng-app="myApp" ng-controller="dataController">
                <form>
                    <h2 style="color:#E99002; text-align: center;">
                    <i style="float:left" class="material-icons">people</i>   Chat App   <i style="float:right" class="material-icons">people</i>
                    </h2>
                    <br/>

                    <div class="form-group">
                        <select class="form-control" ng-model="receiver" required>
                            <option value="">Send to..</option>
                            <option ng-repeat="contact in contacts" value="{{contact.username}}">{{contact.username}}</option>
                        </select>
                    </div>

                    <div id="chatBox" class="form-control">
                        <div ng-repeat="message in messages" ng-style="message.sender == {{sender}} && {'text-align': 'right'}" ng-cloak>
                            <p ng-style="{ padding: message.sender == {{sender}} ? '0 5px 0 35%' : '0 35% 0 5px' }">
                                {{message.content}}
                            </p>
                            <h6>{{message.sendingTime.substring(0, message.sendingTime.lastIndexOf(':'))}}</h6>
                            <hr />
                        </div>
                    </div>
                    <br />

                    <div class="form-group">
                        <label for="message">Message:</label>
                        <input type="text" ng-model="sentMessage" class="form-control" placeholder="Type your message here...">
                    </div>
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#logoutModal">
                        Logout <i class="material-icons">exit_to_app</i>
                    </button>
                    <div class="form-group" id="buttonSubmit">
                        <input type="submit" class="btn btn-warning" ng-click="sendMessage()" value="Send" />
                    </div>

                </form>
            </div>

            <div class="modal fade" id="logoutModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content" style="background-color:#293A4A">
                        <div class="modal-header" style="color:#E99002">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Logout</h4>
                        </div>
                        <div class="modal-body">
                            <h3 style="color:#C0CAD3">Are you sure you want to logout?</h3>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" onclick="window.location='logout.php';">Yes</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                        </div>                           
                    </div>
                </div>
            </div>

        <script>
            var app = angular.module('myApp', []);
            app.controller('dataController', function ($scope, $http, $interval, $timeout, $filter) {
                $scope.sender = <?php echo $_SESSION['userId'] ?>;
                $scope.receiver = "";
                $scope.receiverId = 0;
                $scope.sentMessage = "";
                $scope.messages = [];
                $scope.contacts = [];

                $scope.scrollToBottom = function () {
                    var chatBox = $('#chatBox');
                    chatBox.scrollTop(chatBox.prop("scrollHeight"));
                }

                $scope.sendMessage = function () {
                    if ($scope.sentMessage.trim() != "") {
                        $http.post('send.php', {
                                'sender': $scope.sender,
                                'receiver': $scope.receiverId,
                                'sentMessage': $scope.sentMessage.trim(),
                                'sendingTime': $filter('date')(new Date(), "HH:mm")
                        })
                            .success(function (data, status, headers, config) {
                                //console.log(data);
                            });
                    };

                    $scope.sentMessage = " ";
                };

                $scope.lastMessageId = 0;



                $scope.updateChat = function () {
                    $interval.cancel($scope.updatingChat);
                    $http.post('updateChat.php', {
                            'sender': $scope.sender,
                            'receiver': $scope.receiverId,
                            'lastId': $scope.lastMessageId
                        })
                        .success(function (data, status, headers, config) {
                            if (data[0] != "") {
                                $scope.lastMessageId = data[data.length - 1].id;
                                $timeout($scope.scrollToBottom, 500);
                                for (var i = 0; i < data.length; i++) {
                                    var lastFiveElements = $scope.messages.slice($scope.messages.length - 6,5);
                                    if($scope.messages.findIndex(x => x.id == data[i].id) == -1){
                                        $scope.messages.push(data[i]);
                                    }                              
                                }
                            }
                        });
                    $scope.updatingChat = $interval($scope.updateChat, 1700);
                };

                $scope.getContacts = function () {
                    $http.get("getContacts.php").success(function (response) {
                        if (response[0] != "") {
                            $timeout($scope.scrollToBottom, 500);
                            for (var i = 0; i < response.length; i++) {
                                $scope.contacts.push(response[i]);
                            }
                        }
                    });
                }

                $scope.$watch('receiver', function () {
                    $scope.lastMessageId = 0;
                    $scope.messages = [];
                    for (var i = 0; i < $scope.contacts.length; i++) {
                        var selectedContact = $scope.contacts[i];
                        if ($scope.receiver != "" && $scope.receiver == selectedContact.username) {
                            $scope.receiverId = selectedContact.id;
                        }
                    }
                });

                $scope.getContacts();
                $scope.updatingChat = $interval($scope.updateChat, 1700);
            });
        </script>

    </body>

    </html>